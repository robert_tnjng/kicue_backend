- Add Drakor Form:
  {
    drakor_exist: '', // id drakor
    judul: '',
    judul_lain: '',
    judul_lokal: '',
    tanggal_rilis: '', // date
    hari_tayang: '',
    kategori: '',
    sutradara: '',
    penulis: '',
    channel: '',
    sinopsis: '',
    sub_id: '', // url
    sub_en: '', // url
    genre: [
      {
        name: '', // id
      }
    ],
    download: [
      {
        provider: '',
        link: '' // url
      }
    ],
    tag: [
      {
        name: ''
      }
    ],
    cast: [
      {
        name: '',
        alias: '',
        status: ''
      }
    ],
    sampul: [
      {
        img: {} // file
      }
    ]
  }
