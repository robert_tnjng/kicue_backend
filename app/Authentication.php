<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Authentication extends Model
{
    use Notifiable;

    protected $table = 'kc_users';
    protected $fillable = ['name', 'email', 'password', 'role_id'];

    public function photo()
    {
        return $this->hasMany('App\UserPhoto', 'user_id');
    }

    public function donation()
    {
        return $this->hasMany('App\Donation', 'user_id');
    }

    public function role()
    {
        return $this->belongsTo('App\Role', 'role_id');
    }

    public function token()
    {
        return $this->hasMany('App\Token', 'user_id', 'id');
    }

    public function pasar()
    {
        return $this->hasMany('App\Pasar', 'user_id');
    }
}
