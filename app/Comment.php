<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'kc_drakors_comments';
    protected $fillable = ['drakor_id', 'nama', 'komentar'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }
}
