<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Donation extends Model
{
    use Notifiable;

    protected $table = 'kc_donation';
    protected $fillable = ['user_id', 'photo_profile', 'jumlah_donasi', 'bukti', 'status'];

    public function user()
    {
        return $this->belongsTo('App\Authentication', 'user_id');
    }
}
