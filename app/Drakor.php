<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Drakor extends Model
{
    protected $table = 'kc_drakors';
    protected $fillable = [
        'judul',
        'judul_lain',
        'judul_lokal',
        'tanggal_rilis',
        'hari_tayang',
        'sutradara',
        'penulis',
        'channel',
        'sinopsis',
        'sub_id',
        'sub_en',
        'episode_total'
    ];

    public function cast()
    {
        return $this->hasMany('App\DrakorCast', 'drakor_id', 'id');
    }

    public function category()
    {
        return $this->hasOne('App\DrakorCategory', 'drakor_id', 'id');
    }

    public function comment()
    {
        return $this->hasMany('App\DrakorComment', 'drakor_id', 'id');
    }

    public function channel()
    {
        return $this->hasMany('App\DrakorChannel', 'drakor_id', 'id');
    }

    public function cover()
    {
        return $this->hasMany('App\DrakorCover', 'drakor_id', 'id');
    }

    public function episode()
    {
        return $this->hasMany('App\DrakorEpisode', 'drakor_id', 'id');
    }

    public function genre()
    {
        return $this->hasMany('App\DrakorGenre', 'drakor_id', 'id');
    }

    public function like()
    {
        return $this->hasMany('App\DrakorLike', 'drakor_id', 'id');
    }

    public function tag()
    {
        return $this->hasMany('App\DrakorTag', 'drakor_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'drakor_id', 'id');
    }

    public function likes()
    {
        return $this->hasMany('App\Like', 'drakor_id', 'id');
    }
}
