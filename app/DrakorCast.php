<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorCast extends Model
{
    protected $table = 'kc_drakors_casts';
    protected $fillable = ['drakor_id', 'nama', 'nama_peran', 'status'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }
}
