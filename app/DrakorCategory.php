<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorCategory extends Model
{
    protected $table = 'kc_drakors_categorys';
    protected $fillable = ['drakor_id', 'category_id'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }

    public function m_category()
    {
        return $this->hasOne('App\MasterCategory', 'id', 'category_id');
    }
}
