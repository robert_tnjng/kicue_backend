<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorChannel extends Model
{
    protected $table = 'kc_drakors_channels';
    protected $fillable = ['drakor_id', 'channel_id'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }

    public function m_channel()
    {
        return $this->hasOne('App\MasterChannel', 'id', 'drakor_id');
    }
}
