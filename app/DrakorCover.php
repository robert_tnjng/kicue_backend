<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorCover extends Model
{
    protected $table = 'kc_drakors_covers';
    protected $fillable = ['drakor_id', 'path'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }
}
