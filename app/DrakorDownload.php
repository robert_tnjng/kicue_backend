<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorDownload extends Model
{
    protected $table = 'kc_drakors_downloads';
    protected $fillable = ['resolusi_id', 'download_id', 'link'];

    public function resolusi()
    {
        return $this->belongsTo('App\DrakorResolusi', 'resolusi_id');
    }

    public function m_download()
    {
        return $this->hasOne('App\MasterDownload', 'id', 'download_id');
    }
}
