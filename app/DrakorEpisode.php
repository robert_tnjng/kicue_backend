<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorEpisode extends Model
{
    protected $table = 'kc_drakors_episodes';
    protected $fillable = ['drakor_id', 'name', 'sub_id', 'sub_en'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }

    public function resolusi()
    {
        return $this->hasMany('App\DrakorResolusi', 'episode_id', 'id');
    }
}
