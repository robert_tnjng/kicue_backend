<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorGenre extends Model
{
    protected $table = 'kc_drakors_genres';
    protected $fillable = ['drakor_id', 'genre_id'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }

    public function m_genre()
    {
        return $this->hasOne('App\MasterGenre', 'id', 'genre_id');
    }
}
