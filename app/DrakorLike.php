<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorLike extends Model
{
    protected $table = 'kc_drakors_likes';
    protected $fillable = ['drakor_id', 'ip_address'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }
}
