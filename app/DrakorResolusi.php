<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorResolusi extends Model
{
    protected $table = 'kc_drakors_resolusi';
    protected $fillable = ['episode_id', 'resolusi_id'];

    public function episode()
    {
        return $this->belongsTo('App\DrakorEpisode', 'episode_id');
    }

    public function provider()
    {
        return $this->hasMany('App\DrakorDownload', 'resolusi_id', 'id');
    }

    public function m_resolusi()
    {
        return $this->hasOne('App\MasterResolusi', 'id', 'resolusi_id');
    }
}
