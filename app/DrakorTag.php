<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrakorTag extends Model
{
    protected $table = 'kc_drakors_tags';
    protected $fillable = ['drakor_id', 'name'];

    public function drakor()
    {
        return $this->belongsTo('App\Drakor', 'drakor_id');
    }
}
