<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Authentication;
use App\Token;
use App\PageToken;
use App\Notifications\Register;
use App\Notifications\ForgotPassword;

class AuthenticationController extends Controller
{
    public function Register(Request $req)
    {
        $rules = [
            'name' => 'required|max:16|min:6',
            'email' => 'required|email',
            'password' => 'required|max:16|min:6'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $check = Authentication::where('email', $req->email)->first();

        if ($check) {
            return response()->json([
                'status' => 'fail',
                'message' => ['email' => Array('Email already registered.')]
            ], 400);
        }

        $save = Authentication::create([
            'name' => $req->name,
            'email' => $req->email,
            'password' => Crypt::encryptString($req->password),
            'role_id' => 4
        ]);

        $save->photo()->create([
            'path' => '',
            'use' => 1
        ]);

        $save->notify(new Register($save));

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Terjadi kesalahan'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'User berhasil di daftarkan'
        ], 200);
    }

    public function Login(Request $req)
    {
        $rules = [
            'email' => 'required',
            'password' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->erros()
            ], 400);
        }

        $check = Authentication::where('email', $req->email)->first();

        if (!$check) {
            return response()->json([
                'status' =>'fail',
                'message' => 'User belum terdaftar'
            ], 400);
        }

        if (Crypt::decryptString($check['password']) != $req->password) {
            return response()->json([
                'status' =>'fail',
                'message' => 'Password salah'
            ], 400);
        }

        $setToken = Token::create([
            'user_id' => $check['id'],
            'token' => Crypt::encryptString($check['id']) . Str::random(50)
        ]);

        if (!$setToken) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Login success',
            'data' => ['token' => $setToken['token']]
        ], 200);
    }

    public function Me(Request $req)
    {
        $token = str_replace('Bearer ', '', $req->bearerToken());
        $tokenCheck = Token::where('kc_users_token.token', $token)->first();
        $photo = $tokenCheck->user->photo()->where('use', 1)->first();

        if (!$tokenCheck || !$photo) {
            response()->json([
                'status' => 'fail',
                'message' => 'Me fail'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Me Success',
            'data' => [
                '_id' => $tokenCheck->user->id,
                'name' => $tokenCheck->user->name,
                'email' => $tokenCheck->user->email,
                'photo' => ($photo['path']) ? url(Storage::url($photo['path'])) : '',
                'role' => [
                    'id' => $tokenCheck->user->role->value_id,
                    'name' => $tokenCheck->user->role->name
                ]
            ]
        ], 200);
    }

    public function Logout(Request $req)
    {
        $token = str_replace('Bearer ', '', $req->bearerToken());
        $tokenCheck = Token::join('kc_users', 'kc_users_token.user_id', 'kc_users.id')->where('kc_users_token.token', $token)->delete();

        if (!$tokenCheck) {
            response()->json([
                'status' => 'fail',
                'message' => 'Logout fail'
            ], 404);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Logout Success'
        ], 200);
    }

    public function EditProfile(Request $req, $id)
    {
        $profile = null;

        if ($req->type == 'all') {
            $rules = ['name' => 'required|max:16|min:6'];
            $validator = Validator::make($req->all(), $rules);
    
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'fail',
                    'message' => $validator->errors()
                ], 400);
            }
    
            $profile = Authentication::where('id', $id)->first();

            $profileOther = Authentication::where([
                ['id', '!=', $id],
                ['email', '=', $req->email]
            ])->first();

            if ($profileOther) {
                return response()->json([
                    'status' => 'fail',
                    'message' => ['email' => Array('Email already registered.')]
                ], 400);
            }

            $profile->update(['name' => $req->name]);

            if ($req->photo != 'null') {
                $photo = ($req->photo) ? $req->photo->store('public') : '';
                $profile->photo()->where('use', 1)->first()->update(['use' => 0]);
                $profile->photo()->create(['path' => $photo, 'use' => 1]);
            }
        } elseif ($req->type == 'role') {
            $profile = Authentication::where('id', $id)->first();

            $profile->update(['role_id' => $req->role]);
        }

        if (!$profile) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Edited successfully'
        ], 200);
    }

    public function TokenPage(Request $req, $token)
    {
        $page = PageToken::where('token', $token)->first();
        
        if (!$page) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Token not found'
            ], 400);
        }

        if ($page['count'] > 0) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Token expired'
            ], 400);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Token valid'
        ], 200);
    }

    public function ForgotPassword(Request $req)
    {
        $rules = ['email' => 'required|email'];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $check = Authentication::where('email', $req->email);
        $user = $check->first();

        if (!$check->count()) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Email tidak terdaftar'
            ], 400);
        }

        $page = PageToken::create([
            'token' => Crypt::encryptString($req->email),
            'count' => 0
        ]);

        $userSend = [
            'user' => $user,
            'page' => $page
        ];

        $user->notify(new ForgotPassword($userSend));

        return response()->json([
            'status' => 'success',
            'message' => 'The data has been sent to your email.'
        ], 200);
    }

    public function ForgotPasswordNew(Request $req, $token)
    {
        $rules = ['password' => 'required|max:16|min:6'];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $emailDecrypt = Crypt::decryptString($token);
        $user = Authentication::where('email', $emailDecrypt)->update(['password' => Crypt::encryptString($req->password)]);
        $page = PageToken::where('token', $token)->update(['count' => 1]);

        if (!$user || !$page) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Password new successfully'
        ], 200);
    }
}
