<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Comment;
use App\Drakor;

class CommentController extends Controller
{
    public function Index(Request $req)
    {
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $skip = (isset($_GET['skip']) && $_GET['skip']) ? $_GET['skip'] : null;
        $sort = (isset($_GET['sort']) && $_GET['sort']) ? $_GET['sort'] : 'desc';
        $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;
        $comment = (isset($_GET['comment']) && $_GET['comment']) ? $_GET['comment'] : null;
        $drakorId = (isset($_GET['drakor_id']) && $_GET['drakor_id']) ? $_GET['drakor_id'] : null;

        $get = Comment::when($skip, function($query, $skip) {
            return $query->offset($skip);
        })->when($limit, function($query, $limit) {
            return $query->limit($limit);
        })->when($name, function($query, $name) {
            return $query->where('nama', 'LIKE', "%$name%");
        })->when($comment, function($query, $comment) {
            return $query->where('komentar', 'LIKE', "%$comment%");
        })->when($drakorId, function($query, $drakorId) {
            return $query->where('drakor_id', $drakorId);
        })->orderBy('created_at', $sort)->paginate($limit);

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = Array();

        foreach ($get->items() as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'name' => $data['nama'],
                'comment' => $data['komentar'],
                'drakor' => [
                    '_id' => $data->drakor->id,
                    'text' => $data->drakor->judul
                ],
                'datetime' => $data['created_at']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $get->total()],
            'data' => $datas
        ], 200);
    }

    public function Add(Request $req, $id)
    {
        $rules = [
            'name' => 'required',
            'comment' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $save = Drakor::where('id', $id)->first();

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Drakor not found.'
            ], 400);
        }

        $create = $save->comments()->create([
            'nama' => $req->name,
            'komentar' => $req->comment
        ]);

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Added successfully',
            'data' => [
                '_id' => $create['id'],
                'name' => $create['nama'],
                'comment' => $create['komentar'],
                'datetime' => $create['created_at']
            ]
        ], 200);
    }

    public function Edit(Request $req, $id)
    {
        $rules = [
            'name' => 'required',
            'comment' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Validation fail'
            ], 400);
        }

        $update = Comment::where('id', $id)->update([
            'nama' => $req->name,
            'komentar' => $req->comment
        ]);

        if (!$update) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Updated fail'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Updated successfully'
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $get = Comment::where('id', $id)->first();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = [
            '_id' => $get['id'],
            'name' => $get['nama'],
            'comment' => $get['komentar'],
            'datetime' => $get['created_at']
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        $delete = Comment::where('id', $id)->delete();

        if (!$delete) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
