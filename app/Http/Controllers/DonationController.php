<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Validator;
use App\Authentication;
use App\Donation;
use App\Notifications\DonationAdd;
use App\Notifications\DonationApprove;
use App\Notifications\DonationDeny;

class DonationController extends Controller
{
    public function Index(Request $req)
    {
        $datas = Array();
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $user_id = (isset($_GET['user_id']) && $_GET['user_id']) ? $_GET['user_id'] : '';
        $status = (isset($_GET['status']) && $_GET['status'] !== 'null') ? $_GET['status'] : null;
        $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;
        $dateStart = (isset($_GET['date_start']) && $_GET['date_start']) ? $_GET['date_start'] : null;
        $dateEnd = (isset($_GET['date_end']) && $_GET['date_end']) ? $_GET['date_end'] : null;
        $date = ((isset($_GET['date_start']) && $_GET['date_start']) && (isset($_GET['date_end']) && $_GET['date_end'])) ? ['date_start' => $dateStart, 'date_end' => $dateEnd] : false;

        $donation = Donation::
            join('kc_users', 'kc_users.id', '=', 'kc_donation.user_id')
            ->select('kc_donation.*')
            ->when($user_id, function($query, $user_id) {
                return $query->where('kc_donation.user_id', $user_id);
            })
            ->when($status, function($query, $status) {
                return $query->where('kc_donation.status', '=', $status);
            })
            ->when($name, function($query, $name) {
                return $query
                    ->where('kc_users.email', 'LIKE', "%$name%")
                    ->orWhere('kc_users.nama', 'LIKE', "%$name%");
            })
            ->when($date, function($query, $date) {
                return $query->whereBetween('kc_donation.created_at', [$date['date_start'], $date['date_end']]);
            })
            ->orderBy('kc_donation.created_at', 'desc')
        ->paginate($limit);

        // fail
        if (!$donation) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // setup data
        foreach ($donation->items() as $data) {
            $photoGet = $data->user->photo()->where('use', 1)->first();
            $photo = ($photoGet && $photoGet['path']) ? url(Storage::url($photoGet['path'])) : '';

            array_push($datas, [
                '_id' => $data['id'],
                'nama' => $data->user->name,
                'email' => $data->user->email,
                'photo_profile' => $photo,
                'jumlah_donasi' => $data['jumlah_donasi'],
                'bukti' => url(Storage::url($data['bukti'])),
                'status' => $data['status'],
                'created_at' => $data['created_at']
            ]);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $donation->total()],
            'data' => $datas
        ], 200);
    }

    public function Add(Request $req)
    {
        $rules = [
            'jumlah_donasi' => 'required',
            'bukti' => 'required|image',
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $buktiMove = $req->bukti->store('public');
        $donation = Authentication::where('id', $req->id)->first();
        $donation_save = $donation->donation()->create([
            'jumlah_donasi' => $req->jumlah_donasi,
            'bukti' => $buktiMove,
            'status' => 1
        ]);

        $data_send = [
            'nama' => $donation['name'],
            'email' => $donation['email'],
            'id_donation' => $donation_save['id'],
            'jumlah_donasi' => $req->jumlah_donasi
        ];

        $donation->notify(new DonationAdd($data_send));

        // fail
        if (!$donation) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }
        
        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Added successfully',
            'data' => $req->all()
        ], 200);
    }

    public function Edit(Request $req, $id)
    {
        $donation = Donation::where('id', $id)->first();

        if (($donation['status'] * 1) == 2) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Data ini tidak bisa diedit'
            ], 400);
        }

        if ($req->type == 'all') { // edit all
            $rules = [
                'jumlah_donasi' => 'required',
                'bukti' => 'required'
            ];
            $validator = Validator::make($req->all(), $rules);
    
            if ($validator->fails()) {
                return response()->json([
                    'status' => 'fail',
                    'message' => $validator->errors()
                ], 400);
            }

            $buktiMove = ($req->bukti != 'null') ? $req->bukti->store('public') : $donation['bukti'];
            $donation->update([
                'jumlah_donasi' => $req->jumlah_donasi,
                'bukti' => $buktiMove,
                'status' => $req->status
            ]);

            // fail
            if (!$donation) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Something wrong.'
                ], 500);
            }
            
            // success
            return response()->json([
                'status' => 'success',
                'message' => 'Edited successfully',
                'data' => $req->all()
            ], 200);
        } elseif ($req->type == 'status') { // edit status
            $rules = ['status' => 'required'];
            $validator = Validator::make($req->all(), $rules);

            $donation->update(['status' => $req->status]);
            $data_send = [
                'nama' => $donation->user['name'],
                'email' => $donation->user['email'],
                'jumlah_donasi' => $req->jumlah_donasi
            ];

            if ($req->status == 2 || $req->status == '2') {
                $donation->user->notify(new DonationApprove($data_send));
            } elseif ($req->status == 3 || $req->status == '3') {
                $donation->user->notify(new DonationDeny($data_send));
            }

            // fail
            if (!$donation) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Something wrong.'
                ], 500);
            }
            
            // success
            return response()->json([
                'status' => 'success',
                'message' => 'Edited successfully',
                'data' => $req->all()
            ], 200);
        }
    }

    public function View(Request $req, $id)
    {
        $datas = null;
        $donation = Donation::where('id', $id)->first();

        // fail
        if (!$donation) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // setup data
        $photoGet = $donation->user->photo()->where('use', 1)->first();
        $photo = ($photoGet && $photoGet['path']) ? url(Storage::url($photoGet['path'])) : '';
        $datas = [
            'nama' => $donation->user->name,
            'email' => $donation->user->email,
            'photo_profile' => $photo,
            'jumlah_donasi' => $donation['jumlah_donasi'],
            'bukti' => url(Storage::url($donation['bukti'])),
            'status' => $donation['status']
        ];

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        // initial
        $donation = Donation::where('id', $id)->delete();

        // fail
        if (!$donation) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
