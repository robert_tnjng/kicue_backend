<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Drakor;
use App\DrakorCast;
use App\DrakorCategory;
use App\DrakorComment;
use App\DrakorCover;
use App\DrakorDownload;
use App\DrakorGenre;
use App\DrakorLike;
use App\DrakorTag;

class DrakorController extends Controller
{
    public function Index(Request $req)
    {
        $datas = Array();
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $category = (isset($_GET['category']) && $_GET['category']) ? $_GET['category'] : null;
        $tayang = (isset($_GET['tayang']) && $_GET['tayang']) ? $_GET['tayang'] : null;
        $genre = (isset($_GET['genre']) && $_GET['genre']) ? $_GET['genre'] : null;
        $judul = (isset($_GET['judul']) && $_GET['judul']) ? $_GET['judul'] : null;
        $date_start = (isset($_GET['date_start']) && $_GET['date_start']) ? $_GET['date_start'] : null;
        $date_end = (isset($_GET['date_end']) && $_GET['date_end']) ? $_GET['date_end'] : null;
        $date = ($date_start && $date_end) ? [$date_start, $date_end] : '';

        $drakor = Drakor::
            join('kc_drakors_categorys', 'kc_drakors.id', '=', 'kc_drakors_categorys.drakor_id')
            ->join('kc_drakors_genres', 'kc_drakors.id', '=', 'kc_drakors_genres.drakor_id')
            ->select('kc_drakors.*')
            ->when($genre, function($query, $genre) {
                return $query->where('kc_drakors_genres.genre_id', '=', $genre);
            })
            ->when($judul, function($query, $judul) {
                return $query
                    ->where('kc_drakors.judul', 'LIKE', "%$judul%")
                    ->orWhere('kc_drakors.judul_lain', 'LIKE', "%$judul%")
                    ->orWhere('kc_drakors.judul_lokal', 'LIKE', "%$judul%");
            })
            ->when($category, function($query, $category) {
                return $query->where('kc_drakors_categorys.category_id', '=', $category);
            })
            ->when($tayang, function($query, $tayang) {
                return $query->where('kc_drakors.hari_tayang', '=', $tayang);
            })
            ->when($date, function($query, $date) {
                return $query->whereBetween('kc_drakors.created_at', $date);
            })
            ->groupBy('kc_drakors.id')
            ->orderBy('kc_drakors.created_at', 'desc')
        ->paginate($limit);

        // fail
        if (!$drakor) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // setup data
        foreach ($drakor->items() as $data) {
            $addDrakorCast = Array();
            // comment
            $addDrakorCover = Array();
            $addDrakorDownload = Array();
            $addDrakorGenre = Array();
            // like
            $addDrakorTag = Array();

            // cast
            foreach ($data->cast as $cast) {
                array_push($addDrakorCast, [
                    'nama' => $cast['nama'],
                    'nama_peran' => $cast['nama_peran'],
                    'status' => $cast['status']
                ]);
            }

            // tag
            foreach ($data->tag as $tag) {
                array_push($addDrakorTag, [
                    'name' => $tag->name
                ]);
            }

            // cover
            foreach ($data->cover as $cover) {
                array_push($addDrakorCover, [
                    'path' => url(Storage::url($cover['path']))
                ]);
            }

            // download
            foreach ($data->episode as $episode) {
                $resolusiTemp = Array();

                // resolusi
                foreach($episode->resolusi as $resolusi) {
                    $providerTemp = Array();

                    // provider
                    foreach($resolusi->provider as $provider) {
                        array_push($providerTemp, [
                            'name' => [
                                '_id' => $provider->m_download->id,
                                'text' => $provider->m_download->name
                            ],
                            'link' => $provider->link
                        ]);
                    }

                    array_push($resolusiTemp, [
                        'name' => [
                            '_id' => $resolusi->m_resolusi->id,
                            'text' => $resolusi->m_resolusi->name
                        ],
                        'provider' => $providerTemp
                    ]);
                }

                array_push($addDrakorDownload, [
                    'episode' => [
                        'name' => $episode->name,
                        'sub_id' => $episode->sub_id,
                        'sub_id' => $episode->sub_id,
                        'resolusi' => $resolusiTemp
                    ]
                ]);
            }

            // genre
            foreach ($data->genre as $genre) {
                array_push($addDrakorGenre, [
                    'name' => [
                        '_id' => $genre->m_genre->id,
                        'text' => $genre->m_genre->name
                    ]
                ]);
            }

            array_push($datas, [
                '_id' => $data['id'],
                'judul' => $data['judul'],
                'judul_lain' => $data['judul_lain'],
                'judul_lokal' => $data['judul_lokal'],
                'tanggal_rilis' => $data['tanggal_rilis'],
                'hari_tayang' => $data['hari_tayang'],
                'sutradara' => $data['sutradara'],
                'penulis' => $data['penulis'],
                'channel' => $data['channel'],
                'sinopsis' => $data['sinopsis'],
                'category' => [
                    '_id' => $data->category->m_category->id,
                    'text' => $data->category->m_category->name
                ],
                'cast' => $addDrakorCast,
                'sampul' => $addDrakorCover,
                'total_episode' => $data['episode_total'],
                'download' => $addDrakorDownload,
                'genre' => $addDrakorGenre,
                'total_comment' => $data->comments()->count(),
                'total_like' => $data->likes()->count(),
                'created_at' => $data['created_at'],
                'updated_at' => $data['updated_at']
            ]);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $drakor->total()],
            'data' => $datas
        ], 200);
    }

    public function Add(Request $req)
    {
        $rules = [
            'judul' => 'required',
            'genre' => 'required',
            'tanggal_rilis' => 'required',
            'hari_tayang' => 'required',
            'kategori' => 'required',
            'sutradara' => 'required',
            'penulis' => 'required',
            'channel' => 'required',
            'sinopsis' => 'required',
            'total_episode' => 'required',
            'cast.*.name' => 'required',
            'cast.*.alias' => 'required',
            'cast.*.status' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        // initial
        $drakor = Drakor::create([
            'judul' => $req->judul,
            'judul_lain' => $req->judul_lain,
            'judul_lokal' => $req->judul_lokal,
            'tanggal_rilis' => $req->tanggal_rilis,
            'hari_tayang' => $req->hari_tayang,
            'sutradara' => $req->sutradara,
            'penulis' => $req->penulis,
            'channel' => $req->channel,
            'sinopsis' => $req->sinopsis,
            'episode_total' => $req->total_episode
        ]);

        // category
        if ($req->kategori) {
            $drakor->category()->create(['category_id' => $req->kategori]);
        }

        // create cast
        if ($req->cast) {
            foreach ($req->cast as $data) {
                $drakor->cast()->create([
                    'nama' => $data['name'],
                    'nama_peran' => $data['alias'],
                    'status' => $data['status']
                ]);
            }
        }

        // create cover
        if ($req->sampul) {
            foreach ($req->sampul as $data) {
                $sampulMove = ($data['img'] != 'null') ? $data['img']->store('public') : null;
                $drakor->cover()->create(['path' => $sampulMove]);
            }
        }

        // create episode | resolusi | download
        if ($req->download) {
            foreach ($req->download as $data) {
                $episode = $drakor->episode()->create([
                    'name' => $data['episode'],
                    'sub_id' => $data['sub_id'],
                    'sub_en' => $data['sub_en']
                ]);

                foreach ($data['resolusi'] as $resolusi) {
                    $resolusiCreate = $episode->resolusi()->create(['resolusi_id' => $resolusi['name']]);

                    foreach ($resolusi['provider'] as $provider) {
                        $resolusiCreate->provider()->create([
                            'download_id' => $provider['name'],
                            'link' => $provider['link']
                        ]);
                    }
                }
            }
        }

        // create genre
        if ($req->genre) {
            foreach ($req->genre as $data) {
                $drakor->genre()->create(['genre_id' => $data['name']]);
            }
        }

        // create tag
        if ($req->tag) {
            if ($req->tag) {
                foreach ($req->tag as $data) {
                    $drakor->tag()->create(['name' => $data['name']]);
                }
            }
        }

        // fail
        if (!$drakor) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }
        
        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Added successfully',
            'data' => $req->all()
        ], 200);
    }

    public function Edit(Request $req, $id)
    {
        $rules = [
            'judul' => 'required',
            'genre' => 'required',
            'tanggal_rilis' => 'required',
            'hari_tayang' => 'required',
            'kategori' => 'required',
            'sutradara' => 'required',
            'penulis' => 'required',
            'channel' => 'required',
            'sinopsis' => 'required',
            'total_episode' => 'required',
            'cast.*.name' => 'required',
            'cast.*.alias' => 'required',
            'cast.*.status' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        // initial data
        $drakor = Drakor::where('id', $id)->first();

        $drakor->update([
            'judul' => $req->judul,
            'judul_lain' => $req->judul_lain,
            'judul_lokal' => $req->judul_lokal,
            'tanggal_rilis' => $req->tanggal_rilis,
            'hari_tayang' => $req->hari_tayang,
            'sutradara' => $req->sutradara,
            'penulis' => $req->penulis,
            'channel' => $req->channel,
            'sinopsis' => $req->sinopsis,
            'episode_total' => $req->total_episode
        ]);

        // update category
        if ($req->kategori) {
            $drakor->category->update(['category_id' => $req->kategori]);
        }
        
        // remove cast & update cast
        if ($req->cast) {
            foreach ($req->cast as $data) {
                if ($data['id']) {
                    $cast = $drakor->cast()->where('id', $data['id'])->first();

                    if ($data['isDel'] == 'false' || $data['isDel'] == 'undefined') {
                        $cast->update([
                            'nama' => $data['name'],
                            'nama_peran' => $data['alias'],
                            'status' => $data['status']
                        ]);
                    } else {
                        $cast->delete();
                    }
                } else {
                    $drakor->cast()->create([
                        'nama' => $data['name'],
                        'nama_peran' => $data['alias'],
                        'status' => $data['status']
                    ]);
                }
            }
        }

        // remove cover & update cover
        if ($req->sampul) {
            foreach ($req->sampul as $data) {
                if ($data['id']) {
                    if ($data['img'] != 'null') {
                        $sampulMove = ($data['img'] != 'null') ? $data['img']->store('public') : null;
                        $sampul = $drakor->cover()->where('id', $data['id'])->first();

                        if ($data['isDel'] == 'false' || $data['isDel'] == 'undefined') {
                            $sampul->update(['path' => $sampulMove]);
                        } else {
                            $sampul->delete();
                        }
                    }
                } else {
                    $sampulMove = ($data['img'] != 'null') ? $data['img']->store('public') : null;
                    $drakor->cover()->create(['path' => $sampulMove]);
                }
            }
        }

        // remove & update episode | resolusi | provider
        if ($req->download) {
            foreach ($req->download as $data) {
                if ($data['id']) {
                    $episode = $drakor->episode()->where('id', $data['id'])->first();

                    if ($data['isDel'] == 'false' || $data['isDel'] == 'undefined') {
                        $episode->update([
                            'name' => $data['episode'],
                            'sub_id' => $data['sub_id'],
                            'sub_en' => $data['sub_en']
                        ]);

                        foreach ($data['resolusi'] as $resolusi) {
                            if ($resolusi['id']) {
                                $resolusiCreate = $episode->resolusi()->where('id', $resolusi['id'])->first();
    
                                if ($resolusi['isDel'] == 'false' || $resolusi['isDel'] == 'undefined') {
                                    $resolusiCreate->update(['resolusi_id' => $resolusi['name']]);

                                    foreach ($resolusi['provider'] as $provider) {
                                        if ($provider['id']) {
                                            $providerCreate = $resolusiCreate->provider()->where('id', $provider['id'])->first();
        
                                            if ($provider['isDel'] == 'false' || $provider['isDel'] == 'undefined') {
                                                $providerCreate->update([
                                                    'download_id' => $provider['name'],
                                                    'link' => $provider['link']
                                                ]);
                                            } else {
                                                $providerCreate->delete();
                                            }
                                        } else {
                                            $resolusiCreate->provider()->create([
                                                'download_id' => $provider['name'],
                                                'link' => $provider['link']
                                            ]);
                                        }
                                    }
                                } else {
                                    $resolusiCreate->delete();
                                }
                            } else {
                                $resolusiCreate = $episode->resolusi()->create(['resolusi_id' => $resolusi['name']]);
        
                                foreach ($resolusi['provider'] as $provider) {
                                    $resolusiCreate->provider()->create([
                                        'download_id' => $provider['name'],
                                        'link' => $provider['link']
                                    ]);
                                }
                            }
                        }
                    } else {
                        $episode->delete();
                    }
                } else {
                    $episode = $drakor->episode()->create([
                        'name' => $data['episode'],
                        'sub_id' => $data['sub_id'],
                        'sub_en' => $data['sub_en']
                    ]);
    
                    foreach ($data['resolusi'] as $resolusi) {
                        $resolusiCreate = $episode->resolusi()->create(['resolusi_id' => $resolusi['name']]);
    
                        foreach ($resolusi['provider'] as $provider) {
                            $resolusiCreate->provider()->create([
                                'download_id' => $provider['name'],
                                'link' => $provider['link']
                            ]);
                        }
                    }
                }
            }
        }

        // remove genre & update genre
        if ($req->genre) {
            foreach ($req->genre as $data) {
                if ($data['id']) {
                    $genre = $drakor->genre()->where('id', $data['id'])->first();

                    if ($data['isDel'] == 'false' || $data['isDel'] == 'undefined') {
                        $genre->update(['genre_id' => $data['name']]);
                    } else {
                        $genre->delete();
                    }
                } else {
                    $drakor->genre()->create(['genre_id' => $data['name']]);
                }
            }
        }

        // remove tag & update tag
        if ($req->tag) {
            foreach ($req->tag as $data) {
                if ($data['name']) {
                    $tag = $drakor->tag()->where('name', $data['name'])->first();

                    if ($tag) {
                        $tag->update(['name' => $data['name']]);
                    } else {
                        $drakor->tag()->create(['name' => $data['name']]);
                    }
                }
            }
        }

        // fail
        if (!$drakor) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Updated successfully',
            'data' => $req->all()
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $datas = null;
        $drakor = Drakor::where('id', $id)->first();

        // fail
        if (!$drakor) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $addDrakorCast = Array();
        // comment
        $addDrakorCover = Array();
        $addDrakorDownload = Array();
        $addDrakorGenre = Array();
        // like
        $addDrakorTag = Array();

        // cast
        foreach ($drakor->cast as $cast) {
            array_push($addDrakorCast, [
                '_id' => $cast['id'],
                'nama' => $cast['nama'],
                'nama_peran' => $cast['nama_peran'],
                'status' => $cast['status']
            ]);
        }

        // tag
        foreach ($drakor->tag as $tag) {
            array_push($addDrakorTag, [
                'name' => $tag->name
            ]);
        }

        // cover
        foreach ($drakor->cover as $cover) {
            array_push($addDrakorCover, [
                '_id' => $cover['id'],
                'path' => url(Storage::url($cover['path']))
            ]);
        }

        // download
        foreach ($drakor->episode as $episode) {
            $resolusiTemp = Array();

            foreach($episode->resolusi as $resolusi) {
                $providerTemp = Array();

                foreach($resolusi->provider as $provider) {
                    array_push($providerTemp, [
                        '_id' => $provider->id,
                        'name' => [
                            '_id' => $provider->m_download->id,
                            'text' => $provider->m_download->name
                        ],
                        'link' => $provider->link
                    ]);
                }

                array_push($resolusiTemp, [
                    '_id' => $resolusi->id,
                    'name' => [
                        '_id' => $resolusi->m_resolusi->id,
                        'text' => $resolusi->m_resolusi->name
                    ],
                    'provider' => $providerTemp
                ]);
            }

            array_push($addDrakorDownload, [
                'episode' => [
                    '_id' => $episode->id,
                    'name' => $episode->name,
                    'sub_id' => $episode->sub_id,
                    'sub_en' => $episode->sub_en,
                    'resolusi' => $resolusiTemp
                ]
            ]);
        }

        // genre
        foreach ($drakor->genre as $genre) {
            array_push($addDrakorGenre, [
                '_id' => $genre->id,
                'name' => [
                    '_id' => $genre->m_genre->id,
                    'text' => $genre->m_genre->name
                ]
            ]);
        }

        $datas = [
            '_id' => $drakor['id'],
            'judul' => $drakor['judul'],
            'judul_lain' => $drakor['judul_lain'],
            'judul_lokal' => $drakor['judul_lokal'],
            'tanggal_rilis' => $drakor['tanggal_rilis'],
            'hari_tayang' => $drakor['hari_tayang'],
            'sutradara' => $drakor['sutradara'],
            'penulis' => $drakor['penulis'],
            'channel' => $drakor['channel'],
            'sinopsis' => $drakor['sinopsis'],
            'category' => [
                '_id' => $drakor->category->m_category->id,
                'text' => $drakor->category->m_category->name
            ],
            'cast' => $addDrakorCast,
            'sampul' => $addDrakorCover,
            'total_episode' => $drakor['episode_total'],
            'download' => $addDrakorDownload,
            'genre' => $addDrakorGenre,
            'total_comment' => $drakor->comments()->count(),
            'total_like' => $drakor->likes()->count(),
            'created_at' => $drakor['created_at'],
            'updated_at' => $drakor['updated_at']
        ];

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        // initial
        $drakor = Drakor::where('id', $id)->delete();

        // fail
        if (!$drakor) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
