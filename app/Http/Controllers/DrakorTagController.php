<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\DrakorTag;

class DrakorTagController extends Controller
{
    public function Index(Request $req)
    {
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;

        $master = DrakorTag::when($name, function($query, $name) {
            return $query->where('name', 'LIKE', "%$name%");
        })->orderBy('created_at', 'desc')->paginate($limit);

        if (!$master) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = Array();

        foreach ($master->items() as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'drakor' => $data->drakor->judul,
                'name' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $master->total()],
            'data' => $datas
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $get = DrakorTag::where('id', $id)->first();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = [
            '_id' => $get['id'],
            'drakor_id' => $get['drakor_id'],
            'name' => $get['name']
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        $delete = DrakorTag::where('id', $id)->delete();

        if (!$delete) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
