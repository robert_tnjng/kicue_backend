<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\Like;
use App\Drakor;

class LikeController extends Controller
{
    public function Index(Request $req)
    {
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $skip = (isset($_GET['skip']) && $_GET['skip']) ? $_GET['skip'] : null;
        $ipAddress = (isset($_GET['ip_address']) && $_GET['ip_address']) ? $_GET['ip_address'] : null;
        $drakorId = (isset($_GET['drakor_id']) && $_GET['drakor_id']) ? $_GET['drakor_id'] : null;

        $get = Like::when($skip, function($query, $skip) {
            return $query->offset($skip);
        })->when($limit, function($query, $limit) {
            return $query->limit($limit);
        })->when($ipAddress, function($query, $ipAddress) {
            return $query->where('ip_address', 'LIKE', "%$ipAddress%");
        })->when($drakorId, function($query, $drakorId) {
            return $query->where('drakor_id', $drakorId);
        })->orderBy('created_at', 'desc')->get();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = Array();

        foreach ($get as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'ip_address' => $data['ip_address']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Add(Request $req, $id)
    {
        $save = Drakor::where('id', $id)->first();

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Drakor not found.'
            ], 400);
        }

        if ($save->likes()->where('ip_address', $req->ip())->count() > 0) {
            return response()->json([
                'status' => 'fail',
                'message' => 'You already like this drakor.'
            ], 400);
        }

        $save->likes()->create(['ip_address' => $req->ip()]);

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Added successfully',
            'data' => null
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $get = Like::where('id', $id)->first();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = [
            '_id' => $get['id'],
            'ip_address' => $get['ip_address']
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        $delete = Like::where('id', $id)->delete();

        if (!$delete) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
