<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use App\MasterDownload;

class MasterDownloadController extends Controller
{
    public function Index(Request $req)
    {
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;

        $master = MasterDownload::when($name, function($query, $name) {
            return $query->where('name', 'LIKE', "%$name%");
        })->orderBy('created_at', 'desc')->paginate($limit);

        if (!$master) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = Array();

        foreach ($master->items() as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'name' => $data['name'],
                'value' => $data['value']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $master->total()],
            'data' => $datas
        ], 200);
    }

    public function Add(Request $req)
    {
        $rules = [
            'name' => 'required',
            'value' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $save = MasterDownload::create([
            'name' => $req->name,
            'value' => $req->value
        ]);

        if (!$save) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Added successfully',
            'data' => null
        ], 200);
    }

    public function Edit(Request $req, $id)
    {
        $rules = [
            'name' => 'required',
            'value' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }

        $update = MasterDownload::where('id', $id)->update([
            'name' => $req->name,
            'value' => $req->value
        ]);

        if (!$update) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Updated fail'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Updated successfully'
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $get = MasterDownload::where('id', $id)->first();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = [
            '_id' => $get['id'],
            'name' => $get['name'],
            'value' => $get['value']
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        $delete = MasterDownload::where('id', $id)->delete();

        if (!$delete) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
