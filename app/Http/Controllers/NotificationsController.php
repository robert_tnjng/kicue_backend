<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Notifications;

class NotificationsController extends Controller
{
    public function Index()
    {
        $datas = Array();
        $data_table = Notifications::whereNull('read_at')->paginate(10);

        if (!$data_table) {
            return response()->json([
                'status' => 'success',
                'message' => 'Data Not Found'
            ], 204);
        }

        foreach ($data_table->items() as $data) {
            $j_encode = json_decode($data['data']);
            $message = null;

            if ($j_encode->type === 11) {
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'read',
                    'name' => 'auth',
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->name . '</strong> talah berhasil <strong>mandaftarkan diri</strong> ke website kita'
                ];
            } else if ($j_encode->type === 12) {
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'read',
                    'name' => 'auth',
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->name . '</strong> talah mengirim <strong>lupa password</strong>'
                ];
            } else if ($j_encode->type === 13) { // donation add
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'link',
                    'name' => 'donasi',
                    'id' => $j_encode->user->id_donation,
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->name . '</strong> talah mengirim <strong>donasi</strong> dengan jumlah donasi <strong>Rp ' . $j_encode->user->jumlah_donasi . ''
                ];
            } else if ($j_encode->type === 14) { // pasar add
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'link',
                    'name' => 'pasar',
                    'id' => $j_encode->user->id_product,
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->name . '</strong> talah mengirim permintaan untuk <strong>membuka pasar</strong> dengan nama pasar <strong>' . $j_encode->user->nama_produk . ''
                ];
            } else if ($j_encode->type === 15) { // pasar bukti
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'link',
                    'name' => 'pasar',
                    'id' => $j_encode->user->id_product,
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->name . '</strong> talah mengirim <strong>bukti pembayaran</strong> atas nama pasar <strong>' . $j_encode->user->nama_produk . ''
                ];
            } else if ($j_encode->type === 16) { // pasar bukti
                $message = [
                    'id_notif' => $data['id'],
                    'type' => 'read',
                    'name' => 'pasar',
                    'id' => $j_encode->user->id_product,
                    'datetime' => $data['created_at'],
                    'read' => 0,
                    'text' => '<strong>' . $j_encode->user->nama_produk . '</strong> talah <strong>habis masa tayang</strong>'
                ];
            }

            if ($message) { array_push($datas, [ 'message' => $message ]); }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'index' => ['total' => $data_table->total()],
            'data' => $datas
        ], 200);
    }

    public function Read($id)
    {
        $notif = Notifications::where('id', $id)->update(['read_at' => now() ]);

        if (!$notif) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Read successfully'
        ], 200);
    }
}
