<?php

namespace App\Http\Controllers;
use App\Role;
use App\Drakor;
use App\MasterCategory;
use App\MasterGenre;
use App\MasterResolusi;
use App\MasterDownload;
use App\MasterChannel;

use Illuminate\Http\Request;

class OptionsController extends Controller
{
    public function Role()
    {
        $datas = Array();
        $datadb = Role::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['value_id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Drakor()
    {
        $datas = Array();
        $datadb = Drakor::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['judul']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Category()
    {
        $datas = Array();
        $datadb = MasterCategory::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Genre()
    {
        $datas = Array();
        $datadb = MasterGenre::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Resolusi()
    {
        $datas = Array();
        $datadb = MasterResolusi::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Download()
    {
        $datas = Array();
        $datadb = MasterDownload::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }

    public function Channel()
    {
        $datas = Array();
        $datadb = MasterChannel::all();

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'text' => $data['name']
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'data' => $datas
        ], 200);
    }
}
