<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use App\Authentication;
use App\Pasar;
use App\PasarCount;
use App\Notifications\PasarAdd;
use App\Notifications\PasarWaiting;
use App\Notifications\PasarReject;
use App\Notifications\PasarPayReject;
use App\Notifications\PasarBukti;
use App\Notifications\PasarApprove;
use App\Notifications\PasarHabis;

class PasarController extends Controller
{
    public function Index()
    {
        {
            $datas = Array();
            $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
            $user_id = (isset($_GET['user_id']) && $_GET['user_id']) ? $_GET['user_id'] : null;
            $status = (isset($_GET['status']) && $_GET['status']) ? $_GET['status'] : null;
            $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;
            $date_start = (isset($_GET['date_start']) && $_GET['date_start']) ? $_GET['date_start'] : null;
            $date_end = (isset($_GET['date_end']) && $_GET['date_end']) ? $_GET['date_end'] : null;
            $date = ($date_start && $date_end) ? [$date_start, $date_end] : '';
    
            $pasar = Pasar::
                join('kc_users', 'kc_pasar.user_id', '=', 'kc_users.id')
                ->select('kc_pasar.*')
                ->when($user_id, function($query, $user_id) {
                    return $query->where('kc_pasar.user_id', '=', $user_id);
                })
                ->when($status, function($query, $status) {
                    return $query->where('kc_pasar.status', '=', $status);
                })
                ->when($name, function($query, $name) {
                    return $query->where('kc_pasar.nama_produk', 'LIKE', "%$name%");
                })
                ->when($date, function($query, $date) {
                    return $query->whereBetween('kc_drakors.created_at', $date);
                })
                ->groupBy('kc_pasar.id')
                ->orderBy('kc_pasar.created_at', 'desc')
            ->paginate($limit);
    
            // fail
            if (!$pasar) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Something wrong.'
                ], 500);
            }
    
            // setup data
            foreach ($pasar->items() as $data) {  
                array_push($datas, [
                    '_id' => $data['id'],
                    'nama_produk' => $data['nama_produk'],
                    'banner_portrait' => url(Storage::url($data['banner_portrait'])),
                    'banner_landscape' => url(Storage::url($data['banner_landscape'])),
                    'link' => $data['link'],
                    'jumlah_pembayaran' => $data['jumlah_pembayaran'],
                    'bukti' => url(Storage::url($data['bukti'])),
                    'jumlah_tayang' => $data['jumlah_tayang'],
                    'mulai_tayang' => $data['mulai_tayang'],
                    'habis_tayang' => $data['habis_tayang'],
                    'sisa_tayang' => $data['sisa_tayang'],
                    'status' => $data['status'],
                    'count' => $data['count'],
                    'user' => [
                        '_id' => $data->user->id,
                        'email' => $data->user->email,
                        'name' => $data->user->name
                    ]
                ]);
            }
    
            // success
            return response()->json([
                'status' => 'success',
                'message' => 'Get success',
                'index' => ['total' => $pasar->total()],
                'data' => $datas
            ], 200);
        }
    }

    public function View($id)
    {
        {
            $pasar = Pasar::where('id', $id)->first();
    
            // fail
            if (!$pasar) {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Something wrong.'
                ], 500);
            }
    
            // setup data
            $data = [
                '_id' => $pasar['id'],
                'nama_produk' => $pasar['nama_produk'],
                'banner_portrait' => url(Storage::url($pasar['banner_portrait'])),
                'banner_landscape' => url(Storage::url($pasar['banner_landscape'])),
                'link' => $pasar['link'],
                'jumlah_pembayaran' => $pasar['jumlah_pembayaran'],
                'bukti' => url(Storage::url($pasar['bukti'])),
                'jumlah_tayang' => $pasar['jumlah_tayang'],
                'mulai_tayang' => $pasar['mulai_tayang'],
                'habis_tayang' => $pasar['habis_tayang'],
                'sisa_tayang' => $pasar['sisa_tayang'],
                'status' => $pasar['status'],
                'count' => $pasar['count'],
                'user' => [
                    '_id' => $pasar->user->id,
                    'email' => $pasar->user->email,
                    'name' => $pasar->user->name
                ]
            ];
    
            // success
            return response()->json([
                'status' => 'success',
                'message' => 'Get success',
                'data' => $data
            ], 200);
        }
    }

    public function Add(Request $req)
    {
        // validator
        $rules = [
            'nama_produk' => 'required',
            'banner_portrait' => 'required|image',
            'banner_landscape' => 'required|image',
            'link' => 'required',
            'jumlah_pembayaran' => 'required'
        ];
        $validator = Validator::make($req->all(), $rules);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'message' => $validator->errors()
            ], 400);
        }
        // ./validator


        // save
        $save = Authentication::where('id', $req->id)->first();
        $banner_portrait = ($req->banner_portrait != 'null') ? $req->banner_portrait->store('public') : null;
        $banner_landscape = ($req->banner_landscape != 'null') ? $req->banner_landscape->store('public') : null;
        $savePasar = $save->pasar()->create([
            'nama_produk' => $req->nama_produk,
            'banner_portrait' => $banner_portrait,
            'banner_landscape' => $banner_landscape,
            'link' => $req->link,
            'jumlah_pembayaran' => $req->jumlah_pembayaran,
            'bukti' => '',
            'jumlah_tayang' => $req->jumlah_tayang,
            'mulai_tayang' => '',
            'habis_tayang' => '',
            'sisa_tayang' => $req->sisa_tayang,
            'status' => 1,
            'count' => 0
        ]);
        // ./save


        // notification & email
        $data_send = [
            'id' => $req->id,
            'email' => $save['email'],
            'name' => $save['name'],
            'id_produk' => $savePasar['id'],
            'nama_produk' => $req->nama_produk,
            'jumlah_pembayaran' => $req->jumlah_pembayaran
        ];
        $save->notify(new PasarAdd($data_send));
        // ./notification & email


        // save error
        if (!$save) {
            return response()->json([
                'status' => 'success',
                'message' => 'Something wrong.'
            ], 500);
        }
        // ./save error


        return response()->json([
            'status' => 'success',
            'message' => 'Add successfully.',
            'data' => $req->all()
        ], 200);
    }
    
    public function Edit(Request $req, $id)
    {
        // update
        $save = Authentication::where('id', $req->id)->first();
        $savePasar = $save->pasar->where('id', $id)->first();

        if ($req->type == 'all') { // rubah semua
            // validator
            $rules = [
                'nama_produk' => 'required',
                'link' => 'required',
                'jumlah_pembayaran' => 'required'
            ];
            $validator = Validator::make($req->all(), $rules);

            if ($validator->fails()) {
                return response()->json([
                    'status' => 'fail',
                    'message' => $validator->errors()
                ], 400);
            }
            // ./validator


            // check status allow
            if ($savePasar['status'] != '1') {
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Pasar dengan status ini tidak bisa dirubah.'
                ], 400);
            }
            // ./check status allow


            $banner_portrait = ($req->banner_portrait != 'null') ? $req->banner_portrait->store('public') : $savePasar['banner_portrait'];
            $banner_landscape = ($req->banner_landscape != 'null') ? $req->banner_landscape->store('public') : $savePasar['banner_landscape'];
            $savePasar->update([
                'nama_produk' => $req->nama_produk,
                'banner_portrait' => $banner_portrait,
                'banner_landscape' => $banner_landscape,
                'link' => $req->link,
                'jumlah_pembayaran' => $req->jumlah_pembayaran,
                'jumlah_tayang' => $req->jumlah_tayang,
                'sisa_tayang' => $req->sisa_tayang
            ]);
        } elseif ($req->type == 'bukti') { // rubah bukti
            if ($savePasar['status'] != '2' && $savePasar['status'] != '5') { // check status allow
                return response()->json([
                    'status' => 'fail',
                    'message' => 'Pasar dengan status ini, tidak dapat melakukan pembayaran.'
                ], 400);
            }

            $bukti = ($req->bukti != 'null') ? $req->bukti->store('public') : $savePasar['bukti'];
            $savePasar->update(['bukti' => $bukti, 'status' => '2.1']);

            $data_send = [
                'id' => $req->id,
                'email' => $save['email'],
                'name' => $save['name'],
                'id_produk' => $id,
                'nama_produk' => $savePasar['nama_produk'],
                'jumlah_pembayaran' => $savePasar['jumlah_pembayaran'],
                'bukti' => $bukti
            ];
            $save->notify(new PasarBukti($data_send));
        } elseif ($req->type == 'status') { // rubah hanya status
            $data_send = [
                'id' => $req->id,
                'email' => $save['email'],
                'name' => $save['name'],
                'id_produk' => $id,
                'nama_produk' => $savePasar['nama_produk'],
                'jumlah_pembayaran' => $savePasar['jumlah_pembayaran'],
                'mulai_tayang' => now()
            ];

            if ($req->status == '2') { // waiting
                $savePasar->update(['status' => $req->status]);
                $save->notify(new PasarWaiting($data_send));
            } elseif ($req->status == '3') { // reject
                $savePasar->update(['status' => $req->status]);
                $save->notify(new PasarReject($data_send));
            } elseif ($req->status == '4') { // approve
                $savePasar->update(['status' => $req->status, 'mulai_tayang' => now()]);
                $save->notify(new PasarApprove($data_send));
            } elseif ($req->status == '5') { // pay reject
                $savePasar->update(['status' => $req->status]);
                $save->notify(new PasarPayReject($data_send));
            }
        }
        // ./update


        // save error
        if (!$save || !$savePasar) {
            return response()->json([
                'status' => 'success',
                'message' => 'Something wrong.'
            ], 500);
        }
        // ./save error


        return response()->json([
            'status' => 'success',
            'message' => 'Update successfully.',
            'data' => $req->all()
        ], 200);
    }

    public function Delete($id)
    {
        // initial
        $pasar = Pasar::where('id', $id)->delete();

        // fail
        if (!$pasar) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        // success
        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }

    public function Iklan()
    {
        $date = now()->format('yy-m-d');
        $pasar_count;
        $pasars;
        $pasar_array = Array();
        $pasar_count = PasarCount::where('created_at', 'LIKE', "%$date%")->first();

        if (!$pasar_count) {
            $pasar_count = PasarCount::create(['count' => 0]);
        } else {
            $pasar_count->update(['count' => ($pasar_count['count'] + 1)]);
        }

        $pasars = Pasar::where([
            ['status', '=', '4'],
            ['sisa_tayang', '>', 0]
        ])
        ->take(5)
        ->skip(($pasar_count['count'] * 5))
        ->get();

        if (!$pasars->count()) { $pasar_count->update(['count' => 0]); }

        $pasars = Pasar::where([
            ['status', '=', '4'],
            ['sisa_tayang', '>', 0]
        ])
        ->take(5)
        ->skip(($pasar_count['count'] * 5))
        ->get();

        if ($pasars->count()) {
            foreach ($pasars as $pasar) {
                if ($pasar->sisa_tayang > 0) {
                    $pasar->update(['sisa_tayang' => ($pasar['sisa_tayang'] - 1)]);

                    if ($pasar->sisa_tayang == 0) {
                        $pasar->update(['habis_tayang' => now(), 'status' => '6']);
                        $data_send = [
                            'id' => $pasar->user->id,
                            'email' => $pasar->user->email,
                            'name' => $pasar->user->name,
                            'id_produk' => $pasar['id'],
                            'nama_produk' => $pasar['nama_produk'],
                            'jumlah_pembayaran' => $pasar['jumlah_pembayaran'],
                            'mulai_tayang' => $pasar['mulai_tayang'],
                            'habis_tayang' => $pasar['habis_tayang']
                        ];
                        $pasar->user->notify(new PasarHabis($data_send));
                    }
                }

                array_push($pasar_array, [
                    '_id' => $pasar['id'],
                    'nama_produk' => $pasar['nama_produk'],
                    'banner_portrait' => url(Storage::url($pasar['banner_portrait'])),
                    'banner_landscape' => url(Storage::url($pasar['banner_landscape'])),
                    'link' => $pasar['link'],
                    'jumlah_pembayaran' => $pasar['jumlah_pembayaran'],
                    'bukti' => url(Storage::url($pasar['bukti'])),
                    'jumlah_tayang' => $pasar['jumlah_tayang'],
                    'mulai_tayang' => $pasar['mulai_tayang'],
                    'habis_tayang' => $pasar['habis_tayang'],
                    'sisa_tayang' => $pasar['sisa_tayang'],
                    'status' => $pasar['status'],
                    'count' => $pasar['count'],
                    'user' => [
                        '_id' => $pasar->user->id,
                        'email' => $pasar->user->email,
                        'name' => $pasar->user->name
                    ]
                ]);
            }
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Get successfully',
            'data' => $pasar_array
        ], 200);
    }
}
