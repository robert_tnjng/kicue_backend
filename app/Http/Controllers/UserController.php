<?php

namespace App\Http\Controllers;
use App\Authentication;

use Illuminate\Http\Request;

class UserController extends Controller
{
    public function Index()
    {
        $datas = Array();
        $limit = (isset($_GET['limit']) && $_GET['limit']) ? $_GET['limit'] : 10;
        $role_id = (isset($_GET['role_id']) && $_GET['role_id']) ? $_GET['role_id'] : null;
        $email = (isset($_GET['email']) && $_GET['email']) ? $_GET['email'] : null;
        $name = (isset($_GET['name']) && $_GET['name']) ? $_GET['name'] : null;

        $datadb = Authentication::
            join('kc_users_roles', 'kc_users.role_id', '=', 'kc_users_roles.value_id')
            ->when($role_id, function($query, $role_id) {
                return $query->where('kc_users_roles.value_id', $role_id);
            })
            ->when($email, function($query, $email) {
                return $query->where('kc_users.email', 'LIKE', "%$email%");
            })
            ->when($name, function($query, $name) {
                return $query->where('kc_users.name', 'LIKE', "%$name%");
            })
            ->select('kc_users.*')
            ->orderBy('kc_users.created_at', 'desc')
        ->paginate($limit);

        if (!$datadb) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        foreach ($datadb as $data) {
            array_push($datas, [
                '_id' => $data['id'],
                'name' => $data['name'],
                'email' => $data['email'],
                'role' => [
                    'id' => $data->role->value_id,
                    'name' => $data->role->name
                ]
            ]);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Role options successfully',
            'index' => ['total' => $datadb->total()],
            'data' => $datas
        ], 200);
    }

    public function View(Request $req, $id)
    {
        $get = Authentication::where('id', $id)->first();

        if (!$get) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        $datas = [
            '_id' => $get['id'],
            'name' => $get['name'],
            'email' => $get['email'],
            'role' => [
                'id' => $get->role->value_id,
                'name' => $get->role->name
            ]
        ];

        return response()->json([
            'status' => 'success',
            'message' => 'Get success',
            'data' => $datas
        ], 200);
    }

    public function Delete(Request $req, $id)
    {
        $delete = Authentication::where('id', $id)->delete();

        if (!$delete) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Something wrong.'
            ], 500);
        }

        return response()->json([
            'status' => 'success',
            'message' => 'Deleted successfully'
        ], 200);
    }
}
