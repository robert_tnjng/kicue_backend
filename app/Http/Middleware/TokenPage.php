<?php

namespace App\Http\Middleware;

use Closure;
use App\PageToken;

class TokenPage
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $page = PageToken::where('token', $request->token)->first();
        
        if (!$page) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Token not found'
            ], 400);
        }

        if ($page['count'] > 0) {
            return response()->json([
                'status' => 'fail',
                'message' => 'Token expired'
            ], 400);
        }
        
        return $next($request);
    }
}
