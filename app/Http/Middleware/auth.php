<?php

namespace App\Http\Middleware;

use Closure;
use App\Authentication;
use App\Token;

class auth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $token = str_replace('Bearer ', '', $request->bearerToken());
        $tokenChek = Token::join('kc_users', 'kc_users_token.user_id', 'kc_users.id')->where('kc_users_token.token', $token)->first();

        if (!$tokenChek) {
            return response()->json([
                'status' => 'fail',
                'message' => 'User logout'
            ], 400);
        }

        return $next($request);
    }
}
