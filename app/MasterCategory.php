<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterCategory extends Model
{
    protected $table = 'kc_m_categorys';
    protected $fillable = ['name', 'value'];
}
