<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterChannel extends Model
{
    protected $table = 'kc_m_channels';
    protected $fillable = ['name', 'value'];
}
