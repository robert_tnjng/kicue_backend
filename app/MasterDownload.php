<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterDownload extends Model
{
    protected $table = 'kc_m_download';
    protected $fillable = ['name', 'value'];
}
