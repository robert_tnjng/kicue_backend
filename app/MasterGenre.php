<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterGenre extends Model
{
    protected $table = 'kc_m_genres';
    protected $fillable = ['name', 'value'];
}
