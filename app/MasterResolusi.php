<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MasterResolusi extends Model
{
    protected $table = 'kc_m_resolusi';
    protected $fillable = ['name', 'value'];
}
