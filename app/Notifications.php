<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notifications extends Model
{
    public $incrementing = false;
    protected $table = 'notifications';
    protected $fillable = ['id', 'read_at'];
}
