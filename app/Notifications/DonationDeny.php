<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DonationDeny extends Notification
{
    use Queueable;

    private $donation;

    public function __construct($donation)
    {
        $this->donation = $donation;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)->view('email.donation_deny', [
            'name' => $this->donation['nama'],
            'total' => number_format($this->donation['jumlah_donasi'], 2)
        ]);
    }
}
