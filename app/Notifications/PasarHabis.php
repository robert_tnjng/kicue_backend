<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasarHabis extends Notification
{
    use Queueable;

    private $pasar;

    public function __construct($pasar)
    {
        $this->pasar = $pasar;
    }

    public function via($notifiable)
    {
        return ['mail', 'database'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)->view('email.pasar_habis', [
            'id' => $this->pasar['id_produk'],
            'name' => $this->pasar['name'],
            'nama_produk' => $this->pasar['nama_produk'],
            'jumlah_pembayaran' => number_format($this->pasar['jumlah_pembayaran'], 2),
            'mulai_tayang' => $this->pasar['mulai_tayang'],
            'habis_tayang' => $this->pasar['habis_tayang']
        ]);
    }

    public function toDatabase($notifiable)
    {
        return [
            'type' => 16,
            'activity' => 'pasar.habis',
            'user' => [
                'id' => $this->pasar['id'],
                'name' => $this->pasar['name'],
                'email' => $this->pasar['email'],
                'id_product' => $this->pasar['id_produk'],
                'nama_produk' => $this->pasar['nama_produk'],
                'jumlah_pembayaran' => number_format($this->pasar['jumlah_pembayaran'], 2),
                'mulai_tayang' => $this->pasar['mulai_tayang'],
                'habis_tayang' => $this->pasar['habis_tayang']
            ]
        ];
    }
}
