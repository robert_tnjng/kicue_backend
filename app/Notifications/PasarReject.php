<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class PasarReject extends Notification
{
    use Queueable;

    private $pasar;

    public function __construct($pasar)
    {
        $this->pasar = $pasar;
    }

    public function via($notifiable)
    {
        return ['mail'];
    }

    public function toMail($notifiable)
    {
        return (new MailMessage)->view('email.pasar_reject', [
            'id' => $this->pasar['id_produk'],
            'name' => $this->pasar['name'],
            'nama_produk' => $this->pasar['nama_produk'],
            'jumlah_pembayaran' => number_format($this->pasar['jumlah_pembayaran'], 2)
        ]);
    }
}
