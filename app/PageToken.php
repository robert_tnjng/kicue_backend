<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageToken extends Model
{
    protected $table = 'kc_page_token';
    protected $fillable = ['token', 'count'];
}
