<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pasar extends Model
{
    protected $table = 'kc_pasar';
    protected $fillable = [
        'user_id',
        'nama_produk',
        'banner_portrait',
        'banner_landscape',
        'link',
        'jumlah_pembayaran',
        'bukti',
        'jumlah_tayang',
        'mulai_tayang',
        'habis_tayang',
        'sisa_tayang',
        'status',
        'count'
    ];

    public function user()
    {
        return $this->belongsTo('App\Authentication', 'user_id');
    }
}
