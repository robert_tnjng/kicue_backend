<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PasarCount extends Model
{
    protected $table = 'kc_pasar_count';
    protected $fillable = ['count', 'created_at'];
}
