<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = 'kc_users_roles';
    protected $fillable = ['value_id', 'name'];

    public function user()
    {
        return $this->hasMany('App\Authentication', 'role_id', 'value_id');
    }
}
