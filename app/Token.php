<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Token extends Model
{
    protected $table = 'kc_users_token';
    protected $fillable = ['user_id', 'token'];

    public function user()
    {
        return $this->belongsTo('App\Authentication', 'user_id');
    }
}
