<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserPhoto extends Model
{
    protected $table = 'kc_users_photo';
    protected $fillable = ['user_id', 'path', 'use'];

    public function user()
    {
        return $this->belongsTo('App\Authentication', 'user_id');
    }
}
