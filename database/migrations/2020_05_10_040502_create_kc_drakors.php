<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_drakors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('judul')->nullable();
            $table->string('judul_lain')->nullable();
            $table->string('judul_lokal')->nullable();
            $table->dateTimeTz('tanggal_rilis')->nullable();
            $table->string('hari_tayang')->nullable();
            $table->string('sutradara')->nullable();
            $table->string('penulis')->nullable();
            $table->string('channel')->nullable();
            $table->longText('sinopsis')->nullable();
            $table->integer('episode_total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors');
    }
}
