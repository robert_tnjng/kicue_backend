<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsDownloads extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_m_resolusi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->bigInteger('value')->nullable();
            $table->timestamps();
        });

        Schema::create('kc_drakors_episodes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drakor_id');
            $table->string('name')->nullable();
            $table->string('sub_id')->nullable();
            $table->string('sub_en')->nullable();
            $table->timestamps();
        });

        Schema::create('kc_drakors_resolusi', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('episode_id')->nullable();
            $table->unsignedBigInteger('resolusi_id')->nullable();
            $table->timestamps();
        });

        Schema::create('kc_drakors_downloads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('resolusi_id');
            $table->unsignedBigInteger('download_id');
            $table->string('link')->nullable();
            $table->timestamps();
        });

        Schema::table('kc_drakors_episodes', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        });

        Schema::table('kc_drakors_resolusi', function (Blueprint $table) {
            $table->foreign('episode_id')->references('id')->on('kc_drakors_episodes')->onDelete('cascade');
        });

        Schema::table('kc_drakors_resolusi', function (Blueprint $table) {
            $table->foreign('resolusi_id')->references('id')->on('kc_m_resolusi')->onDelete('cascade');
        });

        Schema::table('kc_drakors_downloads', function (Blueprint $table) {
            $table->foreign('resolusi_id')->references('id')->on('kc_drakors_resolusi')->onDelete('cascade');
        });

        Schema::table('kc_drakors_downloads', function (Blueprint $table) {
            $table->foreign('download_id')->references('id')->on('kc_m_download')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_downloads');
    }
}
