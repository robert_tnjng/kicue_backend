<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsCategorys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_drakors_categorys', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drakor_id');
            $table->unsignedBigInteger('category_id');
            $table->timestamps();
        });

        Schema::table('kc_drakors_categorys', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        });

        Schema::table('kc_drakors_categorys', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('kc_m_categorys')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_categorys');
    }
}
