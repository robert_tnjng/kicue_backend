<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsCasts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_drakors_casts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drakor_id');
            $table->string('nama')->nullable();
            $table->string('nama_peran')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });

        Schema::table('kc_drakors_casts', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_casts');
    }
}
