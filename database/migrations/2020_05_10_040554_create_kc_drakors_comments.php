<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_drakors_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drakor_id');
            $table->string('nama')->nullable();
            $table->longText('komentar')->nullable();
            $table->timestamps();
        });

        Schema::table('kc_drakors_comments', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_comments');
    }
}
