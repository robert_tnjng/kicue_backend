<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDonation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_donation', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('photo_profile')->nullable();
            $table->string('jumlah_donasi')->nullable();
            $table->string('bukti')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });

        Schema::table('kc_donation', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_donation');
    }
}
