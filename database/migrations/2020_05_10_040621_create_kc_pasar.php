<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcPasar extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_pasar', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('user_id');
            $table->string('nama_produk')->nullable();
            $table->string('banner_portrait')->nullable();
            $table->string('banner_landscape')->nullable();
            $table->longText('link')->nulnlable();
            $table->bigInteger('jumlah_pembayaran')->nullable();
            $table->longText('bukti')->nullable();
            $table->bigInteger('jumlah_tayang')->nullable();
            $table->bigInteger('sisa_tayang')->nullable();
            $table->timestamps('mulai_tayang')->nullable();
            $table->timestamps('habis_tayang')->nullable();
            $table->string('status')->nullable();
            $table->bigInteger('count')->nullable();
            $table->timestamps();
        });

        Schema::table('kc_pasar', function (Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('kc_users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_pasar');
    }
}
