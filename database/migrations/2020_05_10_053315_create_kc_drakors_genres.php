<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsGenres extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kc_drakors_genres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('drakor_id');
            $table->unsignedBigInteger('genre_id');
            $table->timestamps();
        });

        Schema::table('kc_drakors_genres', function (Blueprint $table) {
            $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        });

        Schema::table('kc_drakors_genres', function (Blueprint $table) {
            $table->foreign('genre_id')->references('id')->on('kc_m_genres')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_genres');
    }
}
