<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKcDrakorsResolusi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('kc_drakors_resolusi', function (Blueprint $table) {
        //     $table->bigIncrements('id');
        //     $table->unsignedBigInteger('drakor_id');
        //     $table->unsignedBigInteger('resolusi_id')->nullable();
        //     $table->timestamps();
        // });

        // Schema::table('kc_drakors_resolusi', function (Blueprint $table) {
        //     $table->foreign('drakor_id')->references('id')->on('kc_drakors')->onDelete('cascade');
        // });

        // Schema::table('kc_drakors_resolusi', function (Blueprint $table) {
        //     $table->foreign('resolusi_id')->references('id')->on('kc_m_resolusi')->onDelete('cascade');
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kc_drakors_resolusi');
    }
}
