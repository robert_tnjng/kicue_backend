<?php

use Illuminate\Database\Seeder;
use App\Drakor;

class DrakorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 100; $i++) {
            $drakor = Drakor::create([
                'judul' => 'Drakor Judul Untuk Web Ini ' . $i,
                'judul_lain' => 'Drakor Judul Lain Untuk Web Ini ' . $i,
                'judul_lokal' => 'Drakor Judul Lokal Untuk Web Ini ' . $i,
                'tanggal_rilis' => '2020-05-02 00:00:00',
                'hari_tayang' => 'Rabu - Kamis',
                'sutradara' => 'Sutradara ' . $i,
                'penulis' => 'Penulis ' . $i,
                'channel' => 1,
                'sinopsis' => 'Lorem ' . $i . ' Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.'
            ]);
            $drakor->category()->create(['category_id' => 1]);
            $drakor->cast()->create([
                'nama' => 'Si A ' . $i,
                'nama_peran' => 'Si Ap ' . $i,
                'status' => 'Pemeran Utama'
            ]);
            $drakor->cover()->create(['path' => null]);
            $episode = $drakor->episode()->create([
                'name' => '1 - 2',
                'sub_id' => 'https://www.youtube.com/watch?v=s0l556P9i-U',
                'sub_en' => 'https://www.youtube.com/watch?v=s0l556P9i-U'
            ]);
            $resolusiCreate = $episode->resolusi()->create([
                'resolusi_id' => 1
            ]);
            $resolusiCreate->provider()->create([
                'download_id' => 1,
                'link' => 'https://www.youtube.com/watch?v=s0l556P9i-U'
            ]);
            $drakor->genre()->create([
                'genre_id' => 1
            ]);
        }
    }
}
