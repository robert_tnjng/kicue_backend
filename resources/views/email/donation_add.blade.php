<div>
  <div>
    Terima kasih kepada, <br>
    - Nama: {{ $name }} <br>
    - Jumlah Donasi: IDR {{ $total }},- <br>
    Donasi anda akan kami tinjau kembali. <br>
    Terima kasih sekali lagi atas donasi yang anda kirim.
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>