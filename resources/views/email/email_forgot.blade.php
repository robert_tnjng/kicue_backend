<div>
  <div>
    Klik link berikut untuk mengatur password baru, <br>
    <a href="{{ env('APP_CLIENT') . 'forgot-password/new/' . $token }}" target="_blank">{{ $token }}</a>
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>