<div>
  <div>
    Terima kasih karena telah membuka pasar pada web kami, <br>
    Deskripsi pasar anda sebagai berikut: <br>

    - Nama Produk: {{ $nama_produk }} <br>
    - Jumlah Pembayaran: IDR {{ $jumlah_pembayaran }},- <br><br>

    Pembukaan pasar anda akan kami tinjau kembali. <br>
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>