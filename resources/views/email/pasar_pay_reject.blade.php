<div>
  <div>
    Terima kasih karena telah membuka pasar pada web kami, <br>
    Deskripsi pasar anda sebagai berikut: <br>

    - Nama Produk: {{ $nama_produk }} <br>
    - Jumlah Pembayaran: IDR {{ $jumlah_pembayaran }},- <br><br>

    Maaf Pembayaran Pasar anda telah kami tolak, karena bukti tidak valid. <br>
    <a href="{{ env('APP_CLIENT') . 'pasar/edit/' . $id }}" target="_blank">Kirim bukti lain</a>
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>