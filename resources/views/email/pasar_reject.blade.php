<div>
  <div>
    Terima kasih karena telah membuka pasar pada web kami, <br>
    Deskripsi pasar anda sebagai berikut: <br>

    - Nama Produk: {{ $nama_produk }} <br>
    - Jumlah Pembayaran: IDR {{ $jumlah_pembayaran }},- <br><br>

    Maaf Pasar anda telah kami tolak, karena tidak sesuai standar. <br>
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>