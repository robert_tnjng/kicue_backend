<div>
  <div>
    Terima kasih karena telah membuka pasar pada web kami, <br>
    Deskripsi pasar anda sebagai berikut: <br>

    - Nama Produk: {{ $nama_produk }} <br>
    - Jumlah Pembayaran: IDR {{ $jumlah_pembayaran }},- <br><br>

    Pasar anda telah kami tinjau, dan pasar anda telah memenuhi standar. <br>
    Silahkan segera lakukan pembayaran agar Pasar anda segera ditayangkan, <br>
    <a href="{{ env('APP_CLIENT') . 'pasar/edit/' . $id }}" target="_blank">Bayar</a>
  </div>

  <br><br><br>

  <div>
    Penuh Hormat, <br>
    KICUE Admin
  </div>
</div>