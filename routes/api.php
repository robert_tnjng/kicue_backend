<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// authentication
Route::prefix('auth')->group(function() {
  Route::post('register', 'AuthenticationController@Register');
  Route::post('login', 'AuthenticationController@Login');
  Route::get('me', 'AuthenticationController@Me')->middleware('j.auth');
  Route::delete('logout', 'AuthenticationController@Logout')->middleware('j.auth');
  Route::post('profile/edit/{id}', 'AuthenticationController@EditProfile')->middleware('j.auth');
  Route::get('token-page/{token}', 'AuthenticationController@TokenPage');
  Route::post('password/forgot', 'AuthenticationController@ForgotPassword');
  Route::patch('password/forgot/{token}', 'AuthenticationController@ForgotPasswordNew')->middleware('j.page.token');
});

// user
Route::prefix('user')->group(function() {
  Route::get('', 'UserController@Index');
  Route::get('/{id}', 'UserController@View')->middleware('j.auth');
  Route::delete('/{id}', 'UserController@Delete')->middleware('j.auth');
});

// master resolusi
Route::prefix('master-resolusi')->group(function() {
  Route::get('', 'MasterResolusiController@Index')->middleware('j.auth');
  Route::post('', 'MasterResolusiController@Add')->middleware('j.auth');
  Route::patch('/{id}', 'MasterResolusiController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'MasterResolusiController@View')->middleware('j.auth');
  Route::delete('/{id}', 'MasterResolusiController@Delete')->middleware('j.auth');
});

// master download
Route::prefix('master-download')->group(function() {
  Route::get('', 'MasterDownloadController@Index')->middleware('j.auth');
  Route::post('', 'MasterDownloadController@Add')->middleware('j.auth');
  Route::patch('/{id}', 'MasterDownloadController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'MasterDownloadController@View')->middleware('j.auth');
  Route::delete('/{id}', 'MasterDownloadController@Delete')->middleware('j.auth');
});

// master category
Route::prefix('master-category')->group(function() {
  Route::get('', 'MasterCategoryController@Index')->middleware('j.auth');
  Route::post('', 'MasterCategoryController@Add')->middleware('j.auth');
  Route::patch('/{id}', 'MasterCategoryController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'MasterCategoryController@View')->middleware('j.auth');
  Route::delete('/{id}', 'MasterCategoryController@Delete')->middleware('j.auth');
});

// master tag
Route::prefix('master-tag')->group(function() {
  Route::get('', 'DrakorTagController@Index')->middleware('j.auth');
  Route::get('/{id}', 'DrakorTagController@View')->middleware('j.auth');
  Route::delete('/{id}', 'DrakorTagController@Delete')->middleware('j.auth');
});

// master genre
Route::prefix('master-genre')->group(function() {
  Route::get('', 'MasterGenreController@Index')->middleware('j.auth');
  Route::post('', 'MasterGenreController@Add')->middleware('j.auth');
  Route::patch('/{id}', 'MasterGenreController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'MasterGenreController@View')->middleware('j.auth');
  Route::delete('/{id}', 'MasterGenreController@Delete')->middleware('j.auth');
});

// master channel
Route::prefix('master-channel')->group(function() {
  Route::get('', 'MasterChannelController@Index')->middleware('j.auth');
  Route::post('', 'MasterChannelController@Add')->middleware('j.auth');
  Route::patch('/{id}', 'MasterChannelController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'MasterChannelController@View')->middleware('j.auth');
  Route::delete('/{id}', 'MasterChannelController@Delete')->middleware('j.auth');
});

// drakor
Route::prefix('drakor')->group(function() {
  Route::get('', 'DrakorController@Index');
  Route::post('', 'DrakorController@Add')->middleware('j.auth');
  Route::post('/{id}', 'DrakorController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'DrakorController@View');
  Route::delete('/{id}', 'DrakorController@Delete')->middleware('j.auth');
});

// comment
Route::prefix('comment')->group(function() {
  Route::get('', 'CommentController@Index');
  Route::post('/{id}', 'CommentController@Add');
  Route::patch('/{id}', 'CommentController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'CommentController@View')->middleware('j.auth');
  Route::delete('/{id}', 'CommentController@Delete')->middleware('j.auth');
});

// like
Route::prefix('like')->group(function() {
  Route::get('', 'LikeController@Index');
  Route::post('/{id}', 'LikeController@Add');
  Route::get('/{id}', 'LikeController@View')->middleware('j.auth');
  Route::delete('/{id}', 'LikeController@Delete')->middleware('j.auth');
});

// donation
Route::prefix('donation')->group(function() {
  Route::get('', 'DonationController@Index');
  Route::post('', 'DonationController@Add')->middleware('j.auth');
  Route::post('/{id}', 'DonationController@Edit')->middleware('j.auth');
  Route::get('/{id}', 'DonationController@View');
  Route::delete('/{id}', 'DonationController@Delete')->middleware('j.auth');
});

// pasar
Route::prefix('pasar')->group(function() {
  Route::get('', 'PasarController@Index');
  Route::get('/{id}', 'PasarController@View');
  Route::post('', 'PasarController@Add')->middleware('j.auth');
  Route::post('/{id}', 'PasarController@Edit')->middleware('j.auth');
  Route::delete('/{id}', 'PasarController@Delete')->middleware('j.auth');
});

// notif
Route::prefix('notif')->group(function() {
  Route::get('', 'NotificationsController@Index')->middleware('j.auth');
  Route::patch('/{id}', 'NotificationsController@Read')->middleware('j.auth');
});

// options
Route::prefix('options')->group(function() {
  Route::get('/role', 'OptionsController@Role');
  Route::get('/drakor', 'OptionsController@Drakor');
  Route::get('/category', 'OptionsController@Category');
  Route::get('/genre', 'OptionsController@Genre');
  Route::get('/resolusi', 'OptionsController@Resolusi');
  Route::get('/download', 'OptionsController@Download');
  Route::get('/channel', 'OptionsController@Channel');
});

// iklan
Route::get('iklan', 'PasarController@Iklan');
